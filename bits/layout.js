
extend(ShareAVilla)
({
    view: function(trgt,butn)
    {
        if (trgt.startsWith("/")){ trgt = trgt.slice(1) };
        let dlim = (trgt.includes("?") ? "?" : "#");
        let prts = trgt.split(dlim);  trgt=(prts[0]||"home");
        if (!butn){ butn = select("#"+trgt+"Butn").parentNode };
        if (!trgt.includes(".")){ trgt+=".js" };
        trgt += (prts[1] || "");
        butn.enclan("active");
        select("#mainView").render("/ShareAVilla/view/"+trgt);
    },



    menu:
    {
        show: function(what)
        {
            select("#mainView").insert
            ([
                {panl:"#toglMenu", $:copyOf(this[what])},
            ]);
            
            select("#toglMenu" + proprCase(what)).focus();
        }
        .bind
        ({
            find:
            [
                {grid:"#toglMenuGrid", $:
                [
                    {row:"", $:[{col:".toglMenuHead", $:
                    [
                        {grid:"#findWrap", $:[{row:
                        [
                            {col:".icon .cntrChld .midlChld",  $:[ {icon:"search", size:"1rem"} ]},
                            {col:".text",  $:[ {input:"#toglMenuFind", demo:"Search"} ]},
                        ]}]}
                    ]}]},
                    {row:"", $:[{col:".toglMenuBody", $:
                    [
                        {panl:"#toglMenuBody"},
                    ]}]}
                ]}
            ],
            
            goto:
            [
            ],
        }),



        hide: function()
        {
            remove("#toglMenu");
        },



        goto: function()
        {
            
        },



        find: function(text)
        {
            purl({target:"/ShareAVilla/search", convey:{find:text}, listen:
            {
                loadend(data)
                {
                    if (!isJson(data.body)){ fail(data); return };
                    data = decode.jso(data.body);
                    select("#toglMenuBody").innerHTML = "";
                    
                    data.map((item)=>
                    {
                        // dump(item.theWords);
                        let path = (item.advertID + "/" + item.heroShot);
                        select("#toglMenuBody").insert
                        ([
                            {grid:".findMenuItem", $:[{row:
                            [
                                {col:".findItemHero", $:[{panl:"", style:{backgroundImage:`url("/ShareAVilla/images/${path}")`}}]},
                                {col:".findItemText", $:[{panl:
                                [
                                    {h5:item.location}
                                ]}]},
                            ]}]}
                        ]);
                    });
                },
            }});
        },
    },




    navi: 
    {
        home: function(butn)
        {
            ShareAVilla.view("home",butn);
        },



        menu: function(butn)
        {
            ShareAVilla.menu.show("goto");
        },



        find: function(butn)
        {
            ShareAVilla.menu.show("find");
        },



        info: function(butn)
        {
            ShareAVilla.view("info",butn);
        },
    }, 
});





ordain(".naviCell")
({
    onclick:function()
    {
        select(".naviCell").map((cell)=>{ cell.declan("active") });
        this.enclan("active");
        let action = this.select("i")[0].id.slice(0,4);
        ShareAVilla.navi[action](this);
    }
});





render
([
    {link:"", rel:"stylesheet", href:"/ShareAVilla/bits/aard.css"},

    {layr:"#mainBody", $:
    [
        {panl:"#naviMenu", $:
        [
            {wrap:".naviCell", canFocus:true, $:[ {i:"#homeButn .naviButn .cenmid .icon-home11"} ]},
            {wrap:".naviCell", canFocus:true, $:[ {i:"#infoButn .naviButn .cenmid .icon-info1"} ]},
            {wrap:".naviCell", canFocus:true, $:[ {i:"#findButn .naviButn .cenmid .icon-search-location"} ]},
            {wrap:".naviCell", canFocus:true, $:[ {i:"#menuButn .naviButn .cenmid .icon-menu"} ]},
        ]},
        {panl:"#mainView"},
    ]}
]);





select("#mainView").listen("ready",function()
{
    globVars({mainView:"#mainView"});

    let trgt = location.href.split(HOSTPURL).pop();
    ShareAVilla.view(trgt);
});



listen("click", function click(evnt,trgt,seek)
{
    if (!select("#toglMenu")){ return };
    seek = "toglMenuHead,toglMenuBody";
    trgt = evnt.target;

    if (!trgt.className || !seek.includes(trgt.className)){ trgt = trgt.select("^4") }; // lookup toglMenu
    if (!!trgt.className && seek.includes(trgt.className)){ return }; // clicked on menu
    remove("#toglMenu");
});



listen("typingStop", function userIdle(evnt,trgt)
{
    trgt = evnt.target;
    if (trgt.name !== "toglMenuFind"){ return };
    ShareAVilla.menu.find(trgt.value);
});


                // document.body.addEventListener("click",(trgt)=>
                // {
                //     if (!select("#toglMenu")){ return };
                //     trgt = trgt.target;
                //     dump(trgt.name);
                //     if (trgt.name === "toglMenu"){ return }; // clicked on menu
                //     // trgt = trgt.parentNode.parentNode;
                //     // dump(trgt);
                //     remove("#toglMenu");
                    
                // })

