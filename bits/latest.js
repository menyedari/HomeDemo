

// Featured :: Latest : directly under the nav-bar
// ----------------------------------------------------------------------------------------------------------------------------
    ShareAVilla.viewLatest = function(data)
    {
        this.viewLayr = select("#nowAvail");
        this.picsFold = "/GumTreeFruit/data/pics";
        this.pixCount = data.length;
        
        // dump(data[0]);
        let levl = 90000;

        data.forEach((item,indx)=>
        {
            let temp = (item.heroShot+"").trim();
            item.heroShot = (temp || item.picsList[0].split("/").pop());
            item.heroShot = (this.picsFold+"/"+item.advertID+"/"+item.heroShot);

            let layr = create({layr:`#picsViewLayr${indx} .picsViewLayr`});
            let titl = create({wrap:".picsViewTitl", $:[{h1:item.theTitle}]});
            let pics = create({wrap:".picsViewTiny"});
 
            layr.indx = indx;
            layr.style.backgroundImage = `url("${item.heroShot}")`;
            layr.style.opacity = ((indx===1) ? 1 : 0);
            layr.style.zIndex = ((indx<1) ? (levl+9000) : ((indx<2) ? (levl+8000) : levl));

            item.picsList.filter((shot)=>
            {
                pics.insert({div:".picsTinyItem", style:`background-image:url("${shot}")`});
            });

            layr.insert(titl);
            layr.insert(pics);

            this.viewLayr.insert(layr);
        });

        this.fading([...data.keys()], levl);
        // listen("resizeDone",this.resync);
        // this.resync();
        // select("#picsViewLayr"+this.crntIndx).fadeOut();
    }
    .bind
    ({
        fading: function(keys, levl)
        {
            let uniq = ShareAVilla.uniqueHash, node, next;
            let loop = setInterval(()=>
            {
                if ((uniq !== ShareAVilla.uniqueHash) || !select("#nowAvail"))
                { clearInterval(loop); return };

                node = select("#picsViewLayr"+keys[0]);
                next = select("#picsViewLayr"+keys[1]);
                node.style.zIndex = (levl + 9000);
                next.style.zIndex = (levl + 8000);

                node.fadeIn(0.5).then(()=>
                {
                    if (!select("#nowAvail")){ return };
                    node.style.zIndex = levl;
                    next.style.zIndex = (levl + 9000);
                    next.style.opacity = 0;
                    keys.rotate();
                });

                
            },6000);
        },

        resync: function resync()
        {
        //  let wrap = select("#featured");
        //  wrap.setStyle({height:ScreenInfo().height});
        },
    });
// ----------------------------------------------------------------------------------------------------------------------------




// build view
// ----------------------------------------------------------------------------------------------------------------------------
    purl("/ShareAVilla/latest",(data)=>
    {
        data = decode.jso(data.body);
        ShareAVilla.uniqueHash = hash();
        ShareAVilla.viewLatest(data);
    });
// ----------------------------------------------------------------------------------------------------------------------------
