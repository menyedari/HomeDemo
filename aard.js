
// defn :: ShareAVilla : stem
// ----------------------------------------------------------------------------------------------------------------------------
    extend(MAIN)
    ({
        ShareAVilla:
        {
            init: function()
            {
                // dump("elo");
            },
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: Array.prototype : rotate
// ----------------------------------------------------------------------------------------------------------------------------
    extend(Array.prototype)
    ({
        rotate(next=1)
        {
            let todo = (next * ((next<0) ? -1 : 1));
            for (todo; (todo > 0); todo--)
            {
                if (next>0){ this.unshift(this.pop()) }
                else{ this.push(this.shift()); }
            };
            return this;
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------




// shim :: fading : helpers
// ----------------------------------------------------------------------------------------------------------------------------
    extend(HTMLElement.prototype)
    ({
        fadeOut(time=3)
        {
            return new Promise((done,fail)=>
            {
                let opac = 1;
                let decr = ((1 / 100) / time);
                let timr = setInterval(()=>
                {
                    this.style.opacity = opac;
                    opac -= decr;
                    if (opac <= 0)
                    {
                        this.style.opacity = 0;
                        clearInterval(timr); done();
                    };
                },10);
            });
        },

        fadeIn(time=3)
        {
            return new Promise((done,fail)=>
            {
                let opac = 0;
                let incr = ((1 / 100) / time);
                let timr = setInterval(()=>
                {
                    this.style.opacity = opac;
                    opac += incr;
                    if (opac >= 1)
                    {
                        this.style.opacity = 1;
                        clearInterval(timr); done();
                    };
                },10);
            });
        }
    });
// ----------------------------------------------------------------------------------------------------------------------------
