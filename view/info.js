([
    {section:"#about", $:
    [
        {div:".mdtext", format:"markdown", $:`(~/ShareAVilla/sect/about.md~)`},
    ]},

    {section:"#contact", $:
    [
        {div:".mdtext", format:"markdown", $:`(~/ShareAVilla/sect/contact.md~)`},
    ]},

    {section:"#apply", $:
    [
        {div:".mdtext", format:"markdown", $:`(~/ShareAVilla/sect/apply.md~)`},
        {div:"#applWrap .cntrChld .padnBoth10", $:
        [
            {butn:".dark .cool", $:"apply for accommodation", onclick:function()
            {
                select("#applWrap").render("/ShareAVilla/form/apply.js");
            }},
        ]},
    ]},
])
