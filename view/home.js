([
    {section:"#welcome", $:
    [
        {center:[{img:".banrLogo", src:"/ShareAVilla/bits/logo_words_02.svg"}]},
        {div:".mdtext", format:"markdown", $:`(~/ShareAVilla/sect/welcome.md~)`},
    ]},

    {section:"#featured", $:
    [
        {div:".mdtext", format:"markdown", $:`(~/ShareAVilla/sect/featured.md~)`},
        {div:"#nowAvail", onready:function()
        {
            (~/ShareAVilla/bits/latest.js~)
        }},
    ]},

    {section:"#services", $:
    [
        {div:".mdtext", format:"markdown", $:`(~/ShareAVilla/sect/services.md~)`},
    ]},
])
