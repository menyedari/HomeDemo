<?
namespace Anon;



# tool :: ShareAVilla : stem handler
# ---------------------------------------------------------------------------------------------------------------------------------------------
    class ShareAVilla
    {
        static $meta;



        static function __init()
        {
            permit::fubu();

            if(isin(NAVIPATH,"/view/") || isin(NAVIPATH,"/form/"))
            { finish(NAVIPATH); };

            if(isin(NAVIPATH,"/images/"))
            {
                $path = ("/GumTreeFruit/data/pics/" . stub(NAVIPATH,"/images/")[2]);
                finish($path);
            };
        }


        static function latest()
        {
            $conf = conf("ShareAVilla/autoConf");
            $data = GumTreeFruit::select
            ([
                fetch => "*", 
                order => "modified:DSC", 
                limit => $conf->listLatest
            ]);

            return $data;
        }


        static function search()
        {
            $find = posted("find");
            $resp = GumTreeFruit::select($find);

            return $resp;
        }


        static function __callStatic($name,$args)
        {

        }
    }
# ---------------------------------------------------------------------------------------------------------------------------------------------