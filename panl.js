"use strict";


requires(["/ShareAVilla/bits/aard.css"]);



select("#AnonAppsView").insert
([
   {panl:"#ShareAVillaPanlSlab", contents:
   [
      {grid:".AnonPanlSlab", contents:
      [
         {row:
         [
            {col:".sideMenuView", contents:
            [
               {grid:
               [
                  {row:[{col:".slabMenuHead", contents:"ShareAVilla"}]},
                  {row:[{col:".panlHorzLine", contents:[{hdiv:""}]}]},
                  {row:[{col:'.slabMenuBody', contents:[{grid:
                  [
                     {row:[{col:'#ShareAVillaToolView', contents:[{panl:'#ShareAVillaToolPanl .sideMenuToolPanl', contents:
                     [

                     ]}]}]},
                     {row:[{col:'.panlHorzLine', contents:[{hdiv:''}]}]},
                     {row:[{col:'#ShareAVillaTreeView', contents:[{panl:'#ShareAVillaTreePanl .sideMenuTreePanl', contents:
                     [
                        {treeview:'', source:'/User/treeMenu', uproot:true, listen:
                        {
                           'LeftClick':function(evnt)
                           {
                              let ctrl=evnt.ctrlKey; let shft=evnt.shiftKey;
                              if(ctrl||shft){evnt.stopImmediatePropagation(); evnt.preventDefault(); evnt.stopPropagation();};
                              Anon.ShareAVilla.open(this.info.path,this.info.type,(ctrl?'ctrl':(shft?'shft':VOID)));
                           },
                        }}
                     ]}]}]},
                  ]}]}]},
               ]}
            ]},
            {col:".panlVertDlim", role:"gridFlex", axis:X, target:"<", contents:[{vdiv:""}]},
            {col:
            [
               {grid:
               [
                  {row:[{col:"#ShareAVillaHeadView .slabViewHead", contents:[{tabber:"#ShareAVillaTabber", theme:".dark", target:"#ShareAVillaBodyPanl"}]}]},
                  {row:[{col:".panlHorzLine", contents:[{hdiv:""}]}]},
                  {row:[{col:".slabViewBody", contents:[{panl:"#ShareAVillaBodyPanl"}]}]},
               ]}
            ]},
         ]}
      ]}
   ]}
]);




extend(Anon)
({
   ShareAVilla:
   {
      anew:function(cbf)
      {
      },


      init:function()
      {
         Busy.edit("/ShareAVilla/panl.js",100);
         signal("ShareAVillaAppReady");
      },


      open:function(p)
      {
         dump("TODO :: ShareAVilla.open "+p);
      },
   }
});